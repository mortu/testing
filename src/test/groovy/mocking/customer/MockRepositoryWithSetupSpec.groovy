package mocking.customer

import spock.lang.Specification

import javax.persistence.EntityManager

/**
 * Created by berserk on 04/07/17.
 */
class MockRepositoryWithSetupSpec extends Specification {

    CustomerRepository customerRepository;

    EntityManager entityManager

    def setup() {
        customerRepository = new CustomerRepository()
        entityManager = Stub(EntityManager.class)
        customerRepository.entityManager = entityManager
    }

    def "customer full name is formed from first name and last name"() {
        given: "a customer with example name values"
        Customer sampleCustomer = new Customer()
        sampleCustomer.setFirstName("Susan")
        sampleCustomer.setLastName("Ivanova")

        and: "an entity manager that always returns this customer"

        entityManager.find(Customer.class, 1L) >> sampleCustomer

        when: "we ask for the full name of the customer"
        String fullName = customerRepository.findFullName(1L)

        then: "we get both first and last name"
        fullName == "Susan Ivanova"
    }

    def "customer is not in the database"() {
        given: "the database has no record for the customer"
        entityManager.find(Customer.class, 1L) >> null

        when: "we ask for the full name of the customer"
        String fullName = customerRepository.findFullName(1L)

        then: "the empty string should be returned"
        fullName == ""
    }
}
