package mocking.customer

import spock.lang.Specification

import javax.persistence.EntityManager

/**
 * Created by berserk on 03/07/17.
 */
class MockRepositorySpec extends Specification {
    def "customer full name is formed from first name and last name"() {
        given: "a customer with example name values"
        Customer sampleCustomer = new Customer()
        sampleCustomer.firstName = "Susan"
        sampleCustomer.setLastName("Ivanova")

        and: "an entity manager that always returns this customer"
        EntityManager entityManager = Stub(EntityManager.class)
        entityManager.find(Customer.class, 1L) >> sampleCustomer

        and: "a customer reader which is the class under test"
        CustomerRepository CustomerRepository = new CustomerRepository()
        CustomerRepository.entityManager = entityManager

        when: "we ask for the full name of the customer"
        String fullName = CustomerRepository.findFullName(1L)

        then: "we get both the first and the last name"
        fullName == "Susan Ivanova"
    }
}
