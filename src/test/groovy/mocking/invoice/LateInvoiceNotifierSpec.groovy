package mocking.invoice

import mocking.customer.Customer
import mocking.invoce.EmailSender
import mocking.invoce.InvoiceStorage
import mocking.invoce.LateInvoiceNotifier
import spock.lang.Specification

/**
 * Created by berserk on 04/07/17.
 */
class LateInvoiceNotifierSpec extends Specification {

    LateInvoiceNotifier lateInvoiceNotifier

    EmailSender emailSender
    InvoiceStorage invoiceStorage

    Customer sampleCustomer

    def setup() {
        invoiceStorage = Stub(InvoiceStorage.class)
        emailSender = Mock(EmailSender.class)

        lateInvoiceNotifier = new LateInvoiceNotifier(emailSender, invoiceStorage)

        sampleCustomer = new Customer()
        sampleCustomer.setFirstName("Susan")
        sampleCustomer.setLastName("Ivanova")
    }

    def "a late invoice should trigger an email"() {
        given: "a customer with a late invoice"
        invoiceStorage.hasOutstandingInvoice(sampleCustomer) >> true

        when: "we check if an email should be sent"
        lateInvoiceNotifier.notifyIfLate(sampleCustomer)

        then: "the customer is indeed emailed"
        1 * emailSender.sendEmail(sampleCustomer)
    }

    def "no late invoices"() {
        given: "a customer with good standing"
        invoiceStorage.hasOutstandingInvoice(sampleCustomer) >> false

        when: "we check if an email should be sent"
        lateInvoiceNotifier.notifyIfLate(sampleCustomer)

        then: "an email is never sent out"
        0 * emailSender.sendEmail(sampleCustomer)
    }
}
