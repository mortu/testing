package calculator

import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException

import static org.junit.Assert.*

/**
 * Created by berserk on 01/07/17.
 */

class CalculatorTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none()

    Calculator calculator

    @Before
    void setup() {
        calculator = new Calculator()
    }

    @Test
    void testNewCalculator() {
        exception.expect(IllegalArgumentException.class)
        assertNotNull(calculator)
        calculator.add(10, "a")
    }
}
