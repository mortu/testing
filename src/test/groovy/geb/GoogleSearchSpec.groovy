package geb

import geb.spock.GebSpec
import org.openqa.selenium.Keys
import org.openqa.selenium.chrome.ChromeDriver

/**
 * Created by berserk on 05/07/17.
 */
class GoogleSearchSpec extends GebSpec {

    def static DIEE_WEB_SITE = "http://dipartimenti.unica.it/ingegneriaelettricaedelettronica/"

    def setup() {
        driver = new ChromeDriver()
    }

    def cleanup() {
        driver.close()
    }

    def "Google Page title is Google"() {
        when:
        go "https://www.google.it"

        then:
        page.title == "Google"
    }


    def "Search for DIEE page should produce a lot of results"() {
        given: "Google Search Page"
        go "https://www.google.it"

        when: "User search for DIEE of University of Cagliari"
        $("#tsf").q = "DIEE"

        and: "Button search is clicked"
        $("input", name: "btnK") << Keys.RETURN

        and: "Results are displayed"
        waitFor {
            $("#resultStats").displayed
        }

        then: "There are 1.XXX.XXX results"
        $("#resultStats").text() =~ 'Circa 1'

        and: "First result has the correct link"
        $(".g")[0].find("a")[0].attr("href") == DIEE_WEB_SITE
    }

}