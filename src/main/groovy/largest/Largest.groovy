package largest

/**
 * Created by berserk on 04/07/17.
 */
class Largest {

    def static max(def list) {
        def maximum
        if (!list) {
            throw new RuntimeException("max: Empty List")
        }
        list.each { item ->
            if (item > maximum) {
                maximum = item
            }
        }
        maximum
    }
}
