package calculator

import annotations.ValidateArgs

/**
 * Created by berserk on 01/07/17.
 */
class Calculator {
    def final validArgTypes = [Integer, Float, Double, Long]

    @ValidateArgs
    def add(def ... args) {
        args.each {
            if (it.class in validArgTypes) {
                throw new IllegalArgumentException()
            }
        }
    }
}
