package mocking.invoce

import mocking.customer.Customer

/**
 * Created by berserk on 04/07/17.
 */
class LateInvoiceNotifier {

    final EmailSender emailSender
    final InvoiceStorage invoiceStorage

    LateInvoiceNotifier(final EmailSender emailSenderArg,
                        final InvoiceStorage invoiceStorageArg) {
        emailSender = emailSenderArg
        invoiceStorage = invoiceStorageArg
    }

    void notifyIfLate(Customer customer) {
        if (invoiceStorage.hasOutstandingInvoice(customer)) {
            emailSender.sendEmail(customer)
        }
    }
}
