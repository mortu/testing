package mocking.largest

/**
 * Created by berserk on 03/07/17.
 */
class Largest {

    def static max(list) {
        def maximum
        if (!list) {
            throw new RuntimeException("max: Empty List")
        }
        list.each { item ->
            if (item > maximum) {
                maximum = item
            }
        }
        maximum
    }

}
