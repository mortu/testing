package mocking.customer

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Created by berserk on 03/07/17.
 */
@Entity
class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    String firstName
    String lastName

}
