package mocking.customer

import mocking.customer.Customer

import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

/**
 * Created by berserk on 03/07/17.
 */
class CustomerRepository {

    @PersistenceContext
    EntityManager entityManager

    String findFullName(Long customerID) {
        Customer customer = entityManager.find(Customer.class, customerID)
        customer ? "$customer.firstName $customer.lastName" : ""
    }
}
