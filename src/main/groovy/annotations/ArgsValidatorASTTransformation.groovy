package annotations

import groovy.transform.CompileStatic
import org.codehaus.groovy.ast.ASTNode
import org.codehaus.groovy.ast.MethodNode
import org.codehaus.groovy.ast.Parameter
import org.codehaus.groovy.ast.expr.ArgumentListExpression
import org.codehaus.groovy.ast.expr.ConstantExpression
import org.codehaus.groovy.ast.expr.MethodCallExpression
import org.codehaus.groovy.ast.expr.VariableExpression
import org.codehaus.groovy.ast.stmt.BlockStatement
import org.codehaus.groovy.ast.stmt.ExpressionStatement
import org.codehaus.groovy.ast.stmt.Statement
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.transform.ASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation

/**
 * Created by berserk on 01/07/17.
 */

@CompileStatic
@GroovyASTTransformation(phase = CompilePhase.SEMANTIC_ANALYSIS)
class ArgsValidatorASTTransformation implements ASTTransformation {

    def validArgTypes = [Integer, Float, Double, Long]

    @Override
    void visit(ASTNode[] nodes, SourceUnit sourceUnit) {
        MethodNode method = nodes[1] as MethodNode
        def existingStatements = ((BlockStatement) method.code).statements
        existingStatements.add(0, createArgsValidatorStatement(method))
    }

    private static void createArgsCheckStatement(Parameter[] args) {
        args.each {
            if (it.class in validArgTypes) {
                throw new IllegalArgumentException()
            }
        }
    }

    private static Statement createArgsValidatorStatement(MethodNode methodNode) {
        new ExpressionStatement(
                new MethodCallExpression(
                        new VariableExpression("this"),
                        new ConstantExpression("ArgsValidatorASTTransformation.createArgsCheckStatement"),
                        new ArgumentListExpression(
                                new ConstantExpression(methodNode.parameters)
                        )
                )
        )
    }
}
